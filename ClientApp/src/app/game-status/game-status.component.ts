import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-game-status',
  templateUrl: './game-status.component.html'
})

export class GameStatusComponent {

  public game = {};
  public url: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.url = baseUrl + 'api/game';
    this.Get();
  }


  private Get() {

    this.http.get<GameStatus>(this.url).subscribe(respond => {

      console.log(respond)
      this.game = respond;

    }, error => console.error(error));

  }


  Command(action: string) {
    return this.http.post(this.url, { "Command": action }).subscribe((respond: GameStatus) => {
      console.log(respond)
      this.game = respond;
    }, error => console.error(error));
  }

}

interface GameStatus {
  playerHand: [{ color: string, name: string, value: number }],
  dealerHand: [{ color: string, name: string, value: number }],
  playerHandCount: number,
  winner: string
}
