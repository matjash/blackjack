import { Component, Inject  } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';

@Component({
  selector: 'game-buttons',
  templateUrl: './game-buttons.component.html'
})

export class GameButtonsComponent {

  public game = [];
  public url: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl + 'api/game';
  }


  Hit() {
    return this.http.post(this.url, { "Command": "Hit" }).subscribe((respond: any[]) => {
      console.log(respond)
    }, error => console.error(error));
  }

}

interface GameCommands {
  Command: string;
}
