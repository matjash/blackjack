﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game21.Models
{
    public class Game
    {
        private Deck Deck;
        private Dealer Dealer;
        private Player Player;

        private bool hit { get; set; }
        private bool stand { get; set; }

        public Game()
        {
            NewGame();
        }

        private void NewGame()
        {
            Deck = new Deck(8);
            Dealer = new Dealer();
            Player = new Player();
            hit = false;
            stand = false;

            OpenGame();

        }

        private void OpenGame()
        {
            for (var i = 0; i < 3; i++)
            {
                if (i != 1) { Deck.TakeCard(Player, "Player"); }
                else        { Deck.TakeCard(Dealer, "Dealer"); }
            }
        }

        public bool Blackjack() => (Player.HandCount() == 21) ? true : false;
 
        public void Hit()
        {
            if (!stand && !Player.Busted() && !Player.Blackjack() ) {
                Deck.TakeCard(Player, "Player");
            }
            
        }

        public void Stand()
        {
            stand = true;

            if ( Player.Busted() || Player.Blackjack() )
            {
                return;
            }
              
            while (Dealer.HandCount() <= Player.HandCount())
            {
                if ( Dealer.HandCount() <= Player.HandCount() && 
                     Dealer.HandCount() < 21 )
                {
                    Deck.TakeCard(Dealer, "Dealer");
                }
                else
                {
                    break;
                }
            }

        }

        public Dictionary<string, int> Status()
        {
            var status = new Dictionary<string, int>();

            status.Add("Player", Player.HandCount());
            status.Add("Dealer", Dealer.HandCount());

            return status;
        }

        public string Winner()
        {
            if ( Player.Busted() )
            {
                return "Dealer";
            }
            else if ( Dealer.Busted() || Player.HandCount() == 21 )
            {
                return "Player";
            }
            else if ( !Dealer.Busted() && Dealer.HandCount() > Player.HandCount() )
            {
                return "Dealer";
            }
            else if ( !Dealer.Busted() || !Player.Busted() ) 
            {
                return "None";
            }
            else
            {
                return "None";
            }
        }

       public void Restart()
        {
            NewGame();
        }

        public int GetHandCount(string name)
        {
            switch (name)
            {
                case "Player":
                    return Player.HandCount();

                case "Dealer":
                    return Dealer.HandCount();
            }

            return 0;
        }

        public List<Card> GetCards(string name)
        {
            switch(name)
            {
                case "Player":
                    return Player.Hand;

                case "Dealer":
                    return Dealer.Hand;

            }

            return null;
        }

    }

        public class GameStatus
    {
        public List<Card> PlayerHand = new List<Card>();
        public List<Card> DealerHand = new List<Card>();
        public int PlayerHandCount { get; set; }
        public int DealerHandCount { get; set; }
        public bool Blackjack { get; set; }
        public string Winner { get; set; }

    }
}