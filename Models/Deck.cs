﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Game21.Models
{


    public class Deck
    {
        private List<Card> Cards = new List<Card>();
        public int NumberOfDecks { get; set; }
        private int CountCardsOut = 0;
        public enum CardSuite { D, H, C, S }
        public Dictionary<string, int> CardNameValue = new Dictionary<string, int>()
        {
            { "Two", 2 },
            { "Three", 3 },
            { "Four", 4 },
            { "Five", 5 },
            { "Six", 6 },
            { "Seven", 7 },
            { "Eight", 8 },
            { "Nine", 9 },
            { "Ten", 10 },
            { "Jack", 10 },
            { "Dame", 10 },
            { "King", 10 },
            { "Ace", 1 },
        };

        private List<Card> CardsTest = new List<Card>();

        public List<Card> Test()
        {
            CardsTest.Add(new Card { color = "C", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "D", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "D", name = "Ten", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Two", value = 2 });
            CardsTest.Add(new Card { color = "D", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Nine", value = 8 });
            CardsTest.Add(new Card { color = "D", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Ten", value = 10 });
            CardsTest.Add(new Card { color = "D", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "D", name = "Ten", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Two", value = 2 });
            CardsTest.Add(new Card { color = "D", name = "Ace", value = 1 });
            CardsTest.Add(new Card { color = "C", name = "Nine", value = 8 });
            CardsTest.Add(new Card { color = "D", name = "Ace", value = 1 });

            return CardsTest;
        }


        public Deck(int _numberOfDecks = 1, bool test = false)
        {
            NumberOfDecks = _numberOfDecks;
            if (!test)
            {
                Shuffle();
            }

            if (test)
            {
                Test();
                Cards = CardsTest;
            }

        }

        private List<Card> GenerateDeck()
        {

            var suit = Enum.GetNames(typeof(CardSuite));

            for (var s = 0; s < suit.Length; s++)
            {
                foreach (var c in CardNameValue)
                {
                    Cards.Add(new Card
                    {
                        name = c.Key,
                        value = c.Value,
                        color = suit[s]
                    });
                }
            }

            return Cards;
        }

        private List<Card> GenerateDecks()
        {

            for (var nod = 0; nod < NumberOfDecks; nod++)
            {
                GenerateDeck();
            }

            return Cards;
        }

        private List<Card> Shuffle()
        {
            var list = GenerateDecks();

            // Fisher-Yates
            Random r = new Random();

            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = r.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
            return list;
        }

        // Cleanup writelines for debugging
        public void TakeCard(Player player, string name)
        {
            CountCardsOut++;
            var card = Cards[0];

            player.Hand.Add(Cards[0]);
            if (Cards[0].name == "Ace")
            {
                player.AceCount++;
            }

            player.SumCards = player.SumCards + Cards[0].value;
            player.CalcHandCount();

            Cards.RemoveAt(0);
            //Status(name, card); //DEBUG

        }

        public void TakeCard(Dealer dealer, string name)
        {
            CountCardsOut++;
            var card = Cards[0];

            dealer.Hand.Add(Cards[0]);
            if (Cards[0].name == "Ace")
            {
                dealer.AceCount++;
            }

            dealer.SumCards = dealer.SumCards + Cards[0].value;
            dealer.CalcHandCount();

            Cards.RemoveAt(0);
            //Status(name, card); //DEBUG
        }

        public void Status(string who, Card card)
        {
            Console.WriteLine(String.Concat(
                who + " got: ",
                card.color,
                card.name,
                card.value
            ));
        }

        public int CardsOut() => CountCardsOut;

    }
}