﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Game21.Models
{
    public class Player
    {
        
        public List<Card> Hand = new List<Card>();
        public string Name;
        public int AceCount = new int();
        public int SumCards = new int();
        private int BestHand { get; set; }

        public Player(string name = "Player")
        {
            Name = name;
        }

        public bool Blackjack() => (BestHand == 21) ? true : false;

        public void CalcHandCount()
        {
            var possibleHands = new List<int>();

            var ace = AceCount;

            while (ace >= 0)
            {
                possibleHands.Add(SumCards + (10 * ace));
                ace--;
            }

            //Choose closest to 21
            var compare = 0;
            foreach (var p in possibleHands)
            {
                if (p > compare && p <= 21)
                {
                    compare = p;
                }
            }

            if (compare == 0)
            {
                BestHand = possibleHands.Min();
            }
            else
            {
                BestHand = compare;
            }


        }

        public int HandCount() => BestHand;

        public bool Busted() => (BestHand > 21) ? true : false;

    }

    public class Dealer : Player
    {
        new public string Name;

        public Dealer(string name = "Dealer")
        {
            Name = name;
        }

    }

    public class PlayerStatus
    {
        public List<Card> Hand = new List<Card>();
        public int HandCount { get; set; }
    }    
}