﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Game21.Models
{
    public sealed class Singleton
    {
        private static volatile Game instance;
        private static object syncRoot = new Object();

        private Singleton() { }

        public static Game Game
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new Game();
                    }
                }

                return instance;
            }
        }
    }
}
