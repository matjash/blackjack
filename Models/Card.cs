﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Game21.Models
{
    public class Card
    {

        public string color { get; set; }
        public string name { get; set; }
        public int value { get; set; }

    }
}