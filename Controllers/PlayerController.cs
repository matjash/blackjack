﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Game21.Models;

namespace Game21.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {

        //Get api/player
        [HttpGet]
        public ActionResult<PlayerStatus> GetPlayerHand()
        {
            var status = new PlayerStatus();
            status.Hand = Singleton.Game.GetCards("Player");
            status.HandCount = Singleton.Game.GetHandCount("Player");

            return status;
        }

    }

    [Route("api/[controller]")]
    [ApiController]
    public class DealerController : ControllerBase
    {

        //Get api/dealer
        [HttpGet]
        public ActionResult<PlayerStatus> GetPlayerHand()
        {
            var status = new PlayerStatus();
            status.Hand = Singleton.Game.GetCards("Dealer");
            status.HandCount = Singleton.Game.GetHandCount("Dealer");

            return status;
        }

    }
}