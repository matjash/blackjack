﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Game21.Models;

namespace Game21.Controllers
{
    public class PostAction
    {
        public string Command { get; set; }
    }




    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {

        private GameStatus Status()
        {
            var status = new GameStatus();
            status.PlayerHand = Singleton.Game.GetCards("Player");
            status.DealerHand = Singleton.Game.GetCards("Dealer");
            status.PlayerHandCount = Singleton.Game.GetHandCount("Player");
            status.DealerHandCount = Singleton.Game.GetHandCount("Dealer");
            status.Blackjack = Singleton.Game.Blackjack();
            status.Winner = Singleton.Game.Winner();

            return status;
        }

        //Get api/player
        [HttpGet]
        public ActionResult<GameStatus> GetWinner()
        {
            return Status();
        }


        // POST api/game
        [HttpPost]
        public ActionResult<GameStatus> Post(PostAction action)
        {


            if (ModelState.IsValid) {

                switch (action.Command)
                {
                    case "Hit":
                        {
                            Singleton.Game.Hit();
                            return Status();
                        }
                        
                    case "Stand":
                        {
                            Singleton.Game.Stand();
                            return Status();
                        }

                    case "New":
                        {
                            Singleton.Game.Restart();
                            return Status();
                        }

                }


            }

            return Status();

        }


    }
}